#!/usr/bin/python

import time
import traceback
import datetime
from pathlib import Path
from types import SimpleNamespace
from typing import Any, Callable, Generator, Union, cast
from st110 import serial_manager
from st110 import config
from st110 import utils
from st110.spec import execute_spec
import argparse

parser = argparse.ArgumentParser(
    prog="ST110 LWS tester",
    description="Run configurations on ST110 test jigs",
)


def parse_toml_file_type_arg(file_path: str) -> Path:
    path = Path(file_path)
    if path.suffix != ".toml":
        raise ValueError(
            "only toml file types allowed for setting config; you gave:"
            f" {path.suffix} as file extension."
        )
    return path


def parse_xlsx_file_type_arg(file_path: str) -> Path:
    path = Path(file_path)
    if path.suffix != ".xlsx":
        raise ValueError(
            "only xlsx file types allowed for setting config; you gave:"
            f" {path.suffix} as file extension."
        )
    return path


parser.add_argument(
    "-p",
    "--port",
    dest="port_name",
    help="The port nr to use for comms (default = COM1)",
)
parser.add_argument(
    "-c",
    "--config",
    help="The path to a config file (.toml) to use",
    type=parse_toml_file_type_arg,
)
parser.add_argument(
    "-s", "--spec", help="The path of a spec (.xlsx)", type=parse_xlsx_file_type_arg
)


RFONTIME = datetime.datetime.now()


def Countdown(period, event=""):
    while period >= 0:
        if period >= 1:
            print(event, " ", period, end="   \r")
            time.sleep(1)
            period = period - 1
        else:
            time.sleep(period)
            print(event, "  0", end="     \r")
            period = -1


def DisableAll(s1: serial_manager.SerialManager):
    s1.DisablePG(1, 0)
    s1.DisablePG(2, 0)
    s1.DisablePG(3, 0)
    s1.DisablePG(1, 1)
    s1.DisablePG(2, 1)
    s1.DisablePG(3, 1)
    s1.DisablePG(1, 2)
    s1.DisablePG(2, 2)
    s1.DisablePG(3, 2)
    s1.DisablePG(1, 3)
    s1.DisablePG(2, 3)
    s1.DisablePG(3, 3)


def Test1(s1: serial_manager.SerialManager):
    DisableAll(s1)
    print("LWS 24 Hour Test\n\n ")
    for x in range(0, 1):
        print(" ************* Step " + str(x + 1) + " ******************")

        RFONTIME = datetime.datetime.now()
        print("LWS ON at", RFONTIME)

        # EnablePG(PGno,RequiredPulsewidth [ns],RequiredPRI [us],Detector,HeadNo=0,eventprint=True)

        s1.EnablePG(1, 50, 5, 5, HeadNo=0)
        s1.EnablePG(2, 50, 50, 1, HeadNo=0)

        s1.EnablePG(1, 50, 5, 5, HeadNo=1)
        s1.EnablePG(1, 50, 20, 4, HeadNo=1)

        s1.EnablePG(1, 50, 300, 5, HeadNo=2)
        s1.EnablePG(2, 50, 5, 6, HeadNo=2)

        s1.EnablePG(1, 50, 50, 1, HeadNo=3)
        s1.EnablePG(2, 50, 5, 6, HeadNo=3)

        time.sleep(20)
        DisableAll(s1)

        time.sleep(10)

    utils.PromptEnter(" Test Finished - Press enter to continue")


def Test2(s1: serial_manager.SerialManager):
    DisableAll(s1)
    print("\n\nTest 2\n\n ")
    running = True
    while running:
        print(" ************* Step ******************")

        RFONTIME = datetime.datetime.now()
        print("LWS ON at", RFONTIME)

        # EnablePG(PGno,RequiredPulsewidth [ns],RequiredPRI [us],Detector,HeadNo=0,eventprint=True)

        s1.EnablePG(1, 50, 5, 5, HeadNo=0)
        s1.EnablePG(2, 50, 50, 1, HeadNo=0)

        s1.EnablePG(1, 50, 5, 5, HeadNo=1)
        s1.EnablePG(2, 50, 20, 4, HeadNo=1)

        s1.EnablePG(1, 50, 300, 5, HeadNo=2)
        s1.EnablePG(2, 50, 5, 6, HeadNo=2)

        s1.EnablePG(1, 50, 50, 1, HeadNo=3)
        s1.EnablePG(2, 50, 5, 6, HeadNo=3)

        time.sleep(3)
        DisableAll(s1)

        time.sleep(297)

        input = utils.PromptEnter(" Test Finished - Press enter to Repeat or X to exit")
        if input == "X":
            running = False


# Generator[tuple[Callable[[SerialManager],None],str],None,None]


def get_next_command():
    exit_script = False
    while not exit_script:
        menu = {}
        menu["1"] = ["Test1", "NWLS Overloading 8 Threats"]
        menu["2"] = ["Test2", "NWLS Overloading"]
        menu["98"] = ["DisableAll", "Disable All"]
        menu["99"] = ["Exit", "Exit script"]
        options = menu.keys()
        for entry in options:
            print(entry, menu[entry][0], " ", menu[entry][1])
        selection = utils.PromptEnter("Please Select:")
        if selection == "99":
            exit_script = True
            return
        else:
            command_name = menu[selection][0]
            if command := globals().get(command_name):
                yield command, command_name
            else:
                raise NotImplementedError(command_name)


def execute(
    serial: serial_manager.SerialManager,
    command: Callable[[serial_manager.SerialManager], None],
    command_name: str,
):
    print("\n\n")
    print("Start Test ", command_name)
    print("\n\n")
    command(serial)
    print("\n\n")


def execute_next_step(serial: serial_manager.SerialManager):
    for next_command, command_name in get_next_command():
        try:
            execute(serial, next_command, command_name)
            yield
        except KeyboardInterrupt:
            print("\n\n")
            print("Keyboard Stop")
            print("\n\n")
            DisableAll(serial)
            return


class Stepper:
    def __init__(self, generator: Generator[Any, Any, Any]) -> None:
        self._finished = False
        self._generator = generator

    def __bool__(self):
        return not self._finished

    def step(self):
        try:
            next(self._generator)
        except StopIteration:
            self._finished = True


def main():
    serial = serial_manager.setup_serial()
    step = execute_next_step(serial)
    stepper = Stepper(step)
    while stepper:
        stepper.step()


def main_old():
    port = config.get_port_name()
    serial = serial_manager.SerialManager(port)
    serial.start()
    time.sleep(1)

    menu = {}
    menu["1"] = ["Test1", "NWLS Overloading 8 Threats"]
    menu["2"] = ["Test2", "NWLS Overloading"]
    menu["98"] = ["DisableAll", "Disable All"]
    menu["99"] = ["Exit", "Quit"]

    while True:
        options = menu.keys()
        for entry in options:
            print(entry, menu[entry][0], " ", menu[entry][1])

        selection = utils.PromptEnter("Please Select:")

        if selection == "99":
            break

        try:
            function = menu[selection][0]
            print("\n\n")
            print("Start Test ", menu[selection][0])
            print("\n\n")
            globals()[function](serial)
            print("\n\n")
        except KeyboardInterrupt:
            print("\n\n")
            print("Keyboard Stop")
            print("\n\n")
            DisableAll(serial)
        except Exception:
            traceback.print_exc()
            print("Unknown Option Selected! / Error duing Test Execution")


class Args(SimpleNamespace):
    port_name: Union[None, str]
    config: Union[None, Path]
    spec: Union[None, Path]


def get_args():
    args = cast(Args, parser.parse_args())
    if args.config:
        config.load_config(args.config)
    if args.port_name:
        config.set_port_name(args.port_name)
    return args


def run_spec(spec_file: Path):
    execute_spec(spec_file)


if __name__ == "__main__":
    args = get_args()
    if args.spec:
        run_spec(args.spec)
    main()
