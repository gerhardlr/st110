from contextlib import contextmanager
from st110.serial_manager import SerialManager
from st110 import config


def disable_all(serial: SerialManager):
    for head in config.get_heads():
        for pg in config.get_pgs():
            serial.DisablePG(pg, head)


@contextmanager
def serial_connection(serial: SerialManager):
    disable_all(serial)
    try:
        yield
    finally:
        disable_all(serial)
