from pathlib import Path
from typing import TypedDict
import pandas as pd
from st110.utils import pausible
from st110 import context
from st110 import serial_manager


class Step(TypedDict):
    PGno: int
    RequiredPulsewidth: float
    RequiredPRI: float
    Detector: int
    HeadNo: int


class Spec:
    def __init__(self, spec_file: Path) -> None:
        spec = pd.read_excel(spec_file)
        # check that correct attributes exist in file
        assert all(
            [
                item
                in ["PGno", "RequiredPulsewidth", "RequiredPRI", "Detector", "HeadNo"]
                for item in spec.columns
            ]
        )
        required_types = {
            "PGno": "int64",
            "RequiredPulsewidth": "int64",
            "RequiredPRI": "int64",
            "Detector": "int64",
            "HeadNo": "int64",
        }
        assert all(
            [(spec.dtypes[key] == required_types[key]) for key in required_types.keys()]
        )
        # this means we can cast each index as an _Step
        self._spec = spec

    def get_next_step(self):
        for index in self._spec.index:
            next_spec = self._spec.iloc[index]
            yield Step(
                PGno=next_spec.PGno,
                RequiredPulsewidth=next_spec.RequiredPulsewidth,
                RequiredPRI=next_spec.RequiredPRI,
                Detector=next_spec.Detector,
                HeadNo=next_spec.HeadNo,
            )


def load_spec(file_name: Path):
    return Spec(file_name)


def execute_spec(file_name: Path):
    spec = load_spec(file_name)
    serial = serial_manager.setup_serial()
    print(f"executing steps specified in file {file_name.name}")
    with context.serial_connection(serial):
        for step in spec.get_next_step():
            with pausible():
                serial.EnablePG(**step)
