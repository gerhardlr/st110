from contextlib import contextmanager
from typing import List, Union
import serial
import sys
from threading import Thread
from queue import Queue
import time
from .protocolwrapper import ProtocolWrapper, ProtocolStatus
from . import config

from .utils import calculateheadsetting, addchecksum
from .messages import Messagehdr


@contextmanager
def rtswrapped(serial_device: serial.Serial, clear_time: float):
    if config.rts_cts_enabled():
        serial_device.rts = True
    yield
    time.sleep(0.1)
    if config.rts_cts_enabled():
        serial_device.rts = False


class SerialManager(Thread):
    def __init__(self, device):
        Thread.__init__(self, daemon=True)
        self.settings = {
            "baudrate": 115200,
            "stopbits": serial.STOPBITS_TWO,
            "timeout": 1,
            "xonxoff": False,
            "rtscts": False,
            "dsrdtr": False,
            "write_timeout": None,
            "inter_byte_timeout": None,
            "exclusive": None,
        }
        self.msg_queue = Queue()
        self.kill_received = False  # A flag to indicate thread shutdown
        self.read_num_bytes = 1
        self.retrycount = config.nr_of_transmits()

        try:
            self.ser = serial.Serial()
            self.ser.apply_settings(self.settings)
            self.ser.port = device
            self.ser.open()
        except (serial.SerialException, OSError):
            print("Could not get serial device %s", device)
            sys.exit(0)

    def send(self, cmd):
        for iteration in range(0, self.retrycount + 1):
            if iteration > 0:
                print(f"Retry Sending {iteration}")
            with rtswrapped(self.ser, clear_time=0.1):
                self.ser.write(cmd)
        return True

    def EnablePG(
        self,
        PGno: int,
        RequiredPulsewidth: float,
        RequiredPRI: float,
        Detector: Union[int, List[int]],
        HeadNo: int = 0,
        eventprint: int = True,
    ):
        if eventprint:
            print(
                "Enabled PG",
                PGno,
                ",PW=",
                RequiredPulsewidth,
                "ns, PRI= ",
                RequiredPRI,
                "ms, on detector ",
                Detector,
                "on Head",
                HeadNo,
            )

        # Addr PG 1 2 3 is 0x08 10 18
        PGno += -1
        Addr = [0x08, 0x10, 0x18]
        PGvalue = [0x02, 0x04, 0x08]

        (
            PulsWidthOut,
            PRIClockWord,
            Diodeno,
            PRICountvaluehigherbyte,
            PRICountvaluelowerbyte,
        ) = calculateheadsetting(RequiredPulsewidth, RequiredPRI, Detector)
        # print("PulsWidthOut",hex(PulsWidthOut), "PRIClockWord",hex(PRIClockWord),
        #  "Diodeno",hex(Diodeno), "PRICountvaluehigherbyte",hex(PRICountvaluehigherbyte),
        #  "PRICountvaluelowerbyte", hex(PRICountvaluelowerbyte))

        payload = Messagehdr.build(
            dict(
                STX=14,
                Destination=HeadNo,
                Source=255,
                Command=0xBB,
                lenMSB=0,
                lenLSB=21,
                data=[
                    dict(
                        AD1=Addr[PGno],  # OffsetPulsWidthMSB
                        AD2=0,  # OffsetPulsWidthLSB
                        Value=PulsWidthOut,  # PulsWidthOutPG1
                    ),
                    dict(
                        AD1=Addr[PGno],  # OffsetClockMSB
                        AD2=6,  # OffsetClockLSB
                        Value=PRIClockWord,  # PRIClockWordPG1
                    ),
                    dict(
                        AD1=Addr[PGno],  # OffsetPRIMSBMSB
                        AD2=3,  # OffsetPRIMSBLSB
                        Value=Diodeno,  # PRIWordPG1MSB
                    ),
                    dict(
                        AD1=Addr[PGno],  # OffsetPRIMIDMSB
                        AD2=2,  # OffsetPRIMIDLSB
                        Value=PRICountvaluehigherbyte,  # PRIWordPG1middlebyte
                    ),
                    dict(
                        AD1=Addr[PGno],  # OffsetPRILSBMSB
                        AD2=1,  # OffsetPRILSBLSB
                        Value=PRICountvaluelowerbyte,  # PRIWordPG1LSB
                    ),
                    dict(
                        AD1=112,  # OffsetEnableMSB
                        AD2=0,  # OffsetEnableLSB
                        Value=PGvalue[PGno],  # PG1
                    ),
                    dict(
                        AD1=96,  # OffsetTestProbeAddressmsb
                        AD2=0,  # OffsetTestProbeAddresslsb
                        Value=1,  # D0_1
                    ),
                ],
            )
        )
        payload = addchecksum(payload)
        self.send(payload)

    def DisablePG(self, PGno=1, HeadNo=0, eventprint=True):
        if eventprint:
            print("Disable PG", PGno, "on Head", HeadNo)
        PGno += -1
        Addr = [0x01, 0x02, 0x03]
        payload = Messagehdr.build(
            dict(
                STX=14,
                Destination=HeadNo,
                Source=255,
                Command=187,
                lenMSB=0,
                lenLSB=6,
                data=[
                    dict(
                        AD1=0x58,  #
                        AD2=Addr[PGno],  #
                        Value=0x0,  #
                    ),
                    dict(
                        AD1=0x60,  #
                        AD2=0x00,  #
                        Value=0x00,  #
                    ),
                ],
            )
        )
        payload = addchecksum(payload)
        self.send(payload)

    def SendPing(self, HeadNo=0):
        payload = Messagehdr.build(
            dict(
                STX=14,
                Destination=HeadNo,
                Source=255,
                Command=180,
                lenMSB=0,
                lenLSB=6,
                data=dict(Data0=65, Data1=66, Data2=67, Data3=68, Data4=69, Data5=70),
            )
        )
        payload = addchecksum(payload)
        self.send(payload)

    def run(self):
        if self.ser.is_open:
            print(
                "Serial port opened on %s at %d"
                % (self.ser.port, self.settings["baudrate"])
            )
        else:
            print("CouEnablld not open serial port %s" % self.ser.port)
            return
        pw = ProtocolWrapper()
        self.ser.reset_input_buffer()
        try:
            while not self.kill_received:
                in_data = self.ser.read(self.read_num_bytes)
                if in_data:
                    ret = pw.input(in_data)
                    if ret == ProtocolStatus.MSG_OK:
                        checksum = addchecksum(pw.last_message[:-1])

                        if checksum == pw.last_message:
                            self.retrycount = 0
                        else:
                            print("Checksum Fail on message", pw.last_message)

        except Exception:
            print("END")

    def close(self):
        self.kill_received = True


def setup_serial():
    port = config.get_port_name()
    serial = SerialManager(port)
    serial.start()
    return serial
