import time
from .serial_manager import SerialManager
from . import config


def main():
    port = config.get_port_name()
    s1 = SerialManager(port)
    s1.start()
    time.sleep(1)
    print("Sending Ping 0")
    s1.SendPing(0)
    time.sleep(2)
    print("Sending Ping 1")
    s1.SendPing(1)
    time.sleep(2)
    print("Sending Ping 2")
    s1.SendPing(2)
    time.sleep(2)
    print("Sending Ping 3")
    s1.SendPing(3)
    time.sleep(2)

    # capture PG1,2,3 on command
    #                       50ns  PRIClockWord         PRI High
    # 0E 00 FF BB 00 15  08 00 01  08 06 01  08 03 02  08 02 1E  08 01 83  70 00 02  60 00 01  D3
    # 0E 00 FF BB 00 15  10 00 01  10 06 01  10 03 00  10 02 BD  10 01 08  70 00 04  60 00 01  E7
    # 0E 00 FF BB 00 15  18 00 01  18 06 01  18 03 01  18 02 5B  18 01 8C  70 00 08  60 00 01  80
    #                                       Diode number          PRI Low
    #                     2040      2046       2043     2042        2041
    #                     2550      2556       2553     2552        2551
    #                     4590      4596       4593     4592        4591
    # PG1 2 3 OFF command
    #  0E 00 FF BB 00 06  58 01 00  60 00 00  7B
    #  0E 00 FF BB 00 06  58 02 00  60 00 00  78
    #  0E 00 FF BB 00 06  58 03 00  60 00 00  79
    #                     14791
    #                     14792
    s1.DisablePG(1)
    s1.DisablePG(2)
    s1.DisablePG(3)

    print("max pri")
    s1.EnablePG(1, 50, 838, 6)
    time.sleep(5)

    print("min pri 20us")
    s1.EnablePG(1, 50, 0.02, 5)
    time.sleep(5)
    s1.DisablePG(1)

    for x in range(10, 600, 50):
        print("PRI", x, "ms")
        s1.EnablePG(3, 50, x, 1)
        s1.EnablePG(3, 50, x, 5, HeadNo=3)

        time.sleep(2)
    s1.DisablePG(3)
    s1.DisablePG(3, HeadNo=3)

    # How to use illuminate multiple detectors
    s1.EnablePG(1, 50, 4, [4])
    s1.EnablePG(1, 50, 4, [4], HeadNo=3)
    time.sleep(2)
    s1.EnablePG(1, 50, 4, [3, 4])
    s1.EnablePG(1, 50, 4, [3, 4], HeadNo=3)
    time.sleep(2)
    s1.EnablePG(1, 50, 4, [3])
    s1.EnablePG(1, 50, 4, [3], HeadNo=3)
    time.sleep(2)
    s1.EnablePG(1, 50, 4, [3, 5])
    s1.EnablePG(1, 50, 4, [3, 5], HeadNo=3)
    time.sleep(2)
    s1.EnablePG(1, 50, 4, [5])
    s1.EnablePG(1, 50, 4, [5], HeadNo=3)
    time.sleep(2)
    s1.EnablePG(1, 50, 4, [5, 2])
    s1.EnablePG(1, 50, 4, [5, 2], HeadNo=3)
    time.sleep(2)
    s1.EnablePG(1, 50, 4, [2])
    s1.EnablePG(1, 50, 4, [2], HeadNo=3)
    time.sleep(2)
    s1.EnablePG(1, 50, 4, [1, 2])
    s1.EnablePG(1, 50, 4, [1, 2], HeadNo=3)
    time.sleep(2)
    s1.EnablePG(1, 50, 4, 1)
    s1.EnablePG(1, 50, 4, 1, HeadNo=3)
    time.sleep(2)

    s1.DisablePG(1, HeadNo=3)
    s1.DisablePG(2, HeadNo=3)
    s1.DisablePG(3, HeadNo=3)
    s1.DisablePG(1, HeadNo=3)
    s1.DisablePG(2, HeadNo=3)
    s1.DisablePG(3, HeadNo=3)

    # Test Pulse Widths
    for pw in [50, 70, 85, 170, 200, 320, 480, 500, 640, 1000, 1280, 1600]:
        s1.EnablePG(1, pw, 0.5, 5)
        time.sleep(2)
    s1.DisablePG(1)

    s1.close()
    s1.join()


if __name__ == "__main__":
    main()
