from contextlib import contextmanager
import math
from typing import List, Union
from st110 import config


def PromptEnter(text):
    try:
        return input(text)
    except SyntaxError:
        return ""


@contextmanager
def pausible():
    yield
    if config.pause_between_tests():
        PromptEnter("Press enter to continue")


def addchecksum(payload):
    # Calculate Checksum
    checksum = 0
    for byte in payload[1:]:
        checksum ^= byte
        # print(byte)
    # print (checksum)
    payload += bytes([checksum])
    return payload


def CalculatePWI(Width, Period):
    PWI = math.ceil(Width / Period)
    return PWI


def CalculatePRI(PWI, PRIClockPeriod, PRICountValue, PRIResolution, PulseWidth):
    if PulseWidth == PRIClockPeriod:
        CalculatedPRI = (
            (PWI * PRIClockPeriod)
            + (PRICountValue * PRIResolution)
            + PRIResolution
            + PulseWidth
        )
    else:
        CalculatedPRI = (
            (PWI * PRIClockPeriod) + (PRICountValue * PRIResolution) + PRIResolution
        )
    return CalculatedPRI


def get_pw_settings(PW):  # PW in nano sec
    PulseDef = {
        50: [0.00000002, 0.00000002, 1, 0x01],
        70: [0.00000004, 0.00000002, 2, 0x02],
        85: [0.00000006, 0.00000002, 3, 0x04],
        170: [0.00000016, 0.00000016, 1, 0x21],
        200: [0.0000002, 0.0000002, 10, 0x08],
        320: [0.00000032, 0.00000016, 2, 0x41],
        480: [0.00000048, 0.00000016, 3, 0x24],
        500: [0.0000005, 0.0000002, 25, 0x10],
        640: [0.00000064, 0.00000032, 2, 0x42],
        1000: [0.000001, 0.0000002, 50, 0x00],
        1280: [0.00000128, 0.00000064, 2, 0x62],
        1600: [0.0000016, 0.00000016, 10, 0x28],
    }

    if PW not in PulseDef.keys():
        print("Warning: Pulse width selected not supported - default used")
        PW = 50

    PulseWidth = PulseDef[PW][0]
    PulseWidthClockPeriod = PulseDef[PW][1]
    PulseWidthFactor = PulseDef[PW][2]
    PulsWidthOut = PulseDef[PW][3]

    return PulseWidth, PulseWidthClockPeriod, PulseWidthFactor, PulsWidthOut


def calculateheadsetting(
    RequiredPulsewidth, RequiredPRI, Detector: Union[int, List[int]]
):
    DiodeDef = [0x40, 0x20, 0x10, 0x8, 0x2, 0x4, 0x1, 0x0]
    Diodeno = 0
    if isinstance(Detector, list):  # Accepts list ONLY
        for led in Detector:
            Diodeno += DiodeDef[led - 1]
    else:
        Diodeno = DiodeDef[Detector - 1]

    RequiredPRI = RequiredPRI / 1000  # RequiredPRI in ms

    PulseWidth, PulseWidthClockPeriod, PulseWidthFactor, PulsWidthOut = get_pw_settings(
        RequiredPulsewidth
    )
    # print("PulseWidth",PulseWidth, "PulseWidthClockPeriod",
    # PulseWidthClockPeriod, PulseWidthFactor, PulsWidthOut)
    quit = False
    DutyCycle = 100
    PRIClockOut = None
    PRICountOut = None
    while (quit is False) & (DutyCycle > 10):
        PRIClockPeriod = 0.00000016
        PRIClockOut = 0x01
        PRICountValue = None

        while quit is False:
            PWI = CalculatePWI(PulseWidth, PRIClockPeriod)
            # print("PWI",PWI)
            PRIResolution = 4.0 * PRIClockPeriod

            if RequiredPRI < (
                PulseWidth * 10.0
            ):  # Check 10% Dutycycle. This will stop negative values in calculation
                print("Required PRI less that 10% duty cycle")
                RequiredPRI = PulseWidth * 10.0

            PRICountValued = (
                RequiredPRI - PRIResolution - (PWI * PRIClockPeriod)
            ) / PRIResolution

            PRICountValue = int(round(PRICountValued))
            if (PRICountValue >= 1) & (PRICountValue <= 32766):
                PRICountOut = PRICountValue
                quit = True
            else:
                if PRIClockPeriod == 0.00000016:
                    PRIClockPeriod = 0.00000032
                    PRIClockOut = 0x02
                elif PRIClockPeriod == 0.00000032:
                    PRIClockPeriod = 0.00000064
                    PRIClockOut = 0x03
                elif PRIClockPeriod == 0.00000064:
                    PRIClockPeriod = 0.0000025
                    PRIClockOut = 0x04
                elif PRIClockPeriod == 0.0000025:
                    PRIClockPeriod = 0.00001
                    PRIClockOut = 0x05
                elif PRIClockPeriod == 0.00001:
                    PRIClockPeriod = 0.00004098
                    PRIClockOut = 0x06
                elif PRIClockPeriod == 0.00004098:
                    PRIClockPeriod = 0.00008192
                    PRIClockOut = 0x07
                elif PRIClockPeriod == 0.00008192:
                    if (RequiredPRI < 0.00000144) | (RequiredPRI > 10.737):
                        PRIClockPeriod = 0.00000016
                        PRIClockOut = 0x01
                    quit = False
        assert PRICountValue
        PWI = CalculatePWI(PulseWidth, PRIClockPeriod)
        PRIResolution = 4.0 * PRIClockPeriod
        CalculatedPRI = CalculatePRI(
            PWI, PRIClockPeriod, PRICountValue, PRIResolution, PulseWidth
        )

        # Calculate frequency and duty cycle for display purpuses only
        # Frequency = 1.0 / CalculatedPRI
        DutyCycle = (PulseWidth / (CalculatedPRI + PulseWidth)) * 100.0

        if DutyCycle > 10:
            # Select higher PRI values to reduce Duty Cycle to < 10%
            # NewPRI = (PulseWidth - (PulseWidth * 0.095)) / 0.095
            quit = False

    # PulsWidthOutPG1 = PulsWidthOut
    assert PRIClockOut
    assert PRICountOut
    PRIClockWord = PRIClockOut
    PRIWordPG1 = PRICountOut & 0x7FFF

    PRICountvaluehigherbyte = int((PRIWordPG1 & 0xFF00) >> 8)  # Word 17

    PRICountvaluelowerbyte = PRIWordPG1 & 0xFF  # Word 20

    return (
        PulsWidthOut,
        PRIClockWord,
        Diodeno,
        PRICountvaluehigherbyte,
        PRICountvaluelowerbyte,
    )
