class ProtocolStatus(object):
    START_MSG = "START_MSG"
    IN_MSG = "IN_MSG"
    MSG_OK = "MSG_OK"
    ERROR = "ERROR"


class ProtocolWrapper(object):
    def __init__(self, header=b"\x0e"):
        self.header = header
        self.state = self.WAIT_HEADER
        self.last_message = b""
        self.message_buf = b""
        self.last_error = b""
        self.message_length = -1
        self.length = 0

    def wrap(self, message):
        """Wrap a message with header, footer and DLE according
        to the settings provided in the constructor.
        """
        """ wrapped = self.header
        for b in message:
            if b in (self.header, self.dle):
                wrapped += self.dle + self.after_dle_func(b)
            else:
                wrapped += b
        return wrapped"""

    # internal state
    (WAIT_HEADER, IN_MSG, AFTER_DLE) = range(3)

    def input(self, new_byte):
        """Call this method whenever a new byte is received. It
        returns a ProtocolStatus (see documentation of class
        for info).
        """
        # print('new_byte',new_byte)
        # print('self.header',self.header)
        if self.state == self.WAIT_HEADER:
            if new_byte == self.header:
                self.message_buf += new_byte
                self.state = self.IN_MSG
                return ProtocolStatus.START_MSG
            else:
                self.last_error = "Expected header (0x%02X), got 0x%02X" % (
                    ord(self.header),
                    ord(new_byte),
                )
                return self._finish_msg_error()
        elif self.state == self.IN_MSG:
            # print("IN_MSG", "self.length",self.length,"self.message_length",self.message_length)
            if (self.message_length == -1) & (self.length == 4):
                self.message_length = int.from_bytes(new_byte, "big") + 6
            self.message_buf += new_byte
            self.length += 1
            #  print("IN_MSG", "self.length",self.length,"self.message_length",self.message_length)
            if self.length == self.message_length:
                return self._finish_msg_ok()
            else:
                #                    print("still in message")
                return ProtocolStatus.IN_MSG
        # elif self.state == self.AFTER_DLE:

        #    self.message_buf += self.after_dle_func(new_byte)
        #    self.length += 1
        #    self.state = self.IN_MSG
        #    if self.length == self.message_length:
        #        return self._finish_msg_ok()
        #    else:
        #        return ProtocolStatus.IN_MSG
        else:
            raise AssertionError()

    def _finish_msg_ok(self):
        self.state = self.WAIT_HEADER
        self.last_message = self.message_buf
        self.message_buf = b""
        self.message_length = -1
        self.length = 0
        return ProtocolStatus.MSG_OK

    def _finish_msg_error(self):
        self.state = self.WAIT_HEADER
        self.last_message = b""
        self.message_buf = b""
        self.message_length = -1
        self.length = 0
        return ProtocolStatus.ERROR


if __name__ == "__main__":
    pass
