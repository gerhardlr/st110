#!/usr/bin/python
import time
import traceback
import datetime
from .serial_manager import SerialManager

RFONTIME = datetime.datetime.now()

s1 = SerialManager("COM1")


def PromptEnter(text):
    try:
        input(text)
    except SyntaxError:
        pass


def Countdown(period, event=""):
    while period >= 0:
        if period >= 1:
            print(event, " ", period, end="   \r")
            time.sleep(1)
            period = period - 1
        else:
            time.sleep(period)
            print(event, "  0", end="     \r")
            period = -1


def DisableAll():
    s1.DisablePG(1, 0)
    s1.DisablePG(2, 0)
    s1.DisablePG(3, 0)
    # s1.DisablePG(1,1)
    # s1.DisablePG(2,1)
    # s1.DisablePG(3,1)
    # s1.DisablePG(1,2)
    # s1.DisablePG(2,2)
    # s1.DisablePG(3,2)
    # s1.DisablePG(1,3)
    # s1.DisablePG(2,3)
    # s1.DisablePG(3,3)


def Test1():
    DisableAll()
    print("LWS LESM Library Test\n\n ")
    for x in range(0, 1):
        print(" ************* Step " + str(x + 1) + " ******************")

        RFONTIME = datetime.datetime.now()
        print("LWS ON at", RFONTIME)

        # EnablePG(PGno,RequiredPulsewidth [ns],RequiredPRI [us],Detector,HeadNo=0,eventprint=True)

        s1.EnablePG(1, 50, 1000, 8, HeadNo=0)
        time.sleep(20)
        DisableAll()
        s1.EnablePG(1, 50, 10000, 8, HeadNo=0)
        time.sleep(20)
        DisableAll()
        s1.EnablePG(1, 50, 100000, 8, HeadNo=0)
        time.sleep(20)
        DisableAll()
        s1.EnablePG(2, 50, 100, 6, HeadNo=0)
        time.sleep(20)
        DisableAll()
        s1.EnablePG(3, 50, 2000, 7, HeadNo=0)
        time.sleep(20)
        DisableAll()
        s1.EnablePG(1, 50, 8000, 6, HeadNo=0)
        time.sleep(20)
        DisableAll()
        s1.EnablePG(1, 50, 2000, 5, HeadNo=0)
        time.sleep(20)
        DisableAll()
        s1.EnablePG(1, 50, 12000, 1, HeadNo=0)
        time.sleep(20)
        DisableAll()
        s1.EnablePG(2, 50, 104000, 4, HeadNo=0)
        time.sleep(20)
        DisableAll()
        s1.EnablePG(3, 50, 12000, 1, HeadNo=0)
        time.sleep(20)
        # DisableAll()
        s1.EnablePG(1, 50, 16500, 2, HeadNo=0)
        time.sleep(20)
        DisableAll()
        s1.EnablePG(1, 50, 80000, 3, HeadNo=0)
        time.sleep(20)
        DisableAll()
        s1.EnablePG(1, 50, 104000, 4, HeadNo=0)
        time.sleep(20)
        DisableAll()
        s1.EnablePG(2, 50, 130000, 5, HeadNo=0)
        time.sleep(20)
        DisableAll()
        s1.EnablePG(3, 50, 170000, 1, HeadNo=0)
        time.sleep(20)
        # DisableAll()

        # s1.EnablePG(1,50,5,5,HeadNo=1)
        # s1.EnablePG(1,50,20,4,HeadNo=1)

        # s1.EnablePG(1,50,300,5,HeadNo=2)
        # s1.EnablePG(2,50,5,6,HeadNo=2)

        # s1.EnablePG(1,50,50,1,HeadNo=3)
        # s1.EnablePG(2,50,5,6,HeadNo=3)

        # time.sleep(20)
        DisableAll()

        # time.sleep(10)

    try:
        input(" Test Finished - Press enter to continue")
    except SyntaxError:
        pass


def Test2():
    DisableAll()
    print("\n\nTest 2\n\n ")
    while True:
        print(" ************* Step ******************")

        RFONTIME = datetime.datetime.now()
        print("LWS ON at", RFONTIME)

        # EnablePG(PGno,RequiredPulsewidth [ns],RequiredPRI [us],Detector,HeadNo=0,eventprint=True)

        s1.EnablePG(1, 50, 5, 5, HeadNo=0)
        s1.EnablePG(2, 50, 50, 1, HeadNo=0)

        s1.EnablePG(1, 50, 5, 5, HeadNo=1)
        s1.EnablePG(2, 50, 20, 4, HeadNo=1)

        s1.EnablePG(1, 50, 300, 5, HeadNo=2)
        s1.EnablePG(2, 50, 5, 6, HeadNo=2)

        s1.EnablePG(1, 50, 50, 1, HeadNo=3)
        s1.EnablePG(2, 50, 5, 6, HeadNo=3)

        time.sleep(3)
        DisableAll()

        time.sleep(297)

    try:
        input(" Test Finished - Press enter to continue")
    except SyntaxError:
        pass


if __name__ == "__main__":
    s1 = SerialManager("COM1")
    s1.start()
    time.sleep(1)

    menu = {}
    menu["1"] = ["Test1", "NWLS LESM Library"]
    menu["2"] = ["Test2", "NWLS Overloading 8 Threats"]
    menu["98"] = ["DisableAll", "Disable All"]

    menu["99"] = ["Exit", "Quit"]

    while True:
        options = menu.keys()
        for entry in options:
            print(entry, menu[entry][0], " ", menu[entry][1])

        selection = input("Please Select:")

        if selection == "99":
            break

        try:
            function = menu[selection][0]
            print("\n\n")
            print("Start Test ", menu[selection][0])
            print("\n\n")
            globals()[function]()
            print("\n\n")
        except KeyboardInterrupt:
            print("\n\n")
            print("Keyboard Stop")
            print("\n\n")
            DisableAll()
        except Exception:
            traceback.print_exc()
            print("Unknown Option Selected! / Error duing Test Execution")
