from pathlib import Path
from typing import List, TypedDict, cast
import toml

_DEFAULT_HEADS = [0, 1, 2, 3]
_DEFAULT_PGS = [1, 2, 3]


class Config(TypedDict):
    rts_cts_enabled: bool
    port_name: str
    pause_between_tests: bool
    nr_of_transmits: int
    heads: List[int]
    PGS: List[int]


_config: Config = Config(
    rts_cts_enabled=True,
    port_name="COM1",
    pause_between_tests=True,
    nr_of_transmits=3,
    heads=_DEFAULT_HEADS,
    PGS=_DEFAULT_PGS,
)


def set_heads(heads: List[int]):
    global _config
    _config["heads"] = heads


def get_heads():
    return _config["heads"]


def set_pgs(pgs: List[int]):
    global _config
    _config["PGS"] = pgs


def get_pgs():
    return _config["PGS"]


def set(**config: Config):
    global _config
    updated_config = cast(Config, {**_config, **config})
    _config = updated_config


def rts_cts_enabled() -> bool:
    return _config["rts_cts_enabled"]


def disable_rts_cts():
    global _config
    _config["rts_cts_enabled"] = False


def enable_rts_cts():
    global _config
    _config["rts_cts_enabled"] = True


def set_port_name(port_name: str):
    global _config
    _config["port_name"] = port_name


def get_port_name() -> str:
    return _config["port_name"]


def load_config(path: Path):
    loaded_config = toml.load(path)
    set(**loaded_config)


def pause_between_tests():
    return _config["pause_between_tests"]


def nr_of_transmits():
    return _config["nr_of_transmits"]
