from construct import Struct, Int8ul, Switch, this, Pass

ConfigRequest = Struct("Data0" / Int8ul)

PingRequest = Struct(
    "Data0" / Int8ul,
    "Data1" / Int8ul,
    "Data2" / Int8ul,
    "Data3" / Int8ul,
    "Data4" / Int8ul,
    "Data5" / Int8ul,
)

DataItem = Struct("AD1" / Int8ul, "AD2" / Int8ul, "Value" / Int8ul)

Messagehdr = Struct(
    "STX" / Int8ul,
    "Destination" / Int8ul,
    "Source" / Int8ul,
    "Command" / Int8ul,
    "lenMSB" / Int8ul,
    "lenLSB" / Int8ul,
    "data"
    / Switch(
        lambda ctx: ctx.Command,
        {
            180: PingRequest,  # 0xB4
            183: ConfigRequest,  # 0xB7 Serial EPROM READ
            187: DataItem[this.lenLSB / 3],  # 0xBB Enable PG
        },
        default=Pass,  # <-- sets the default
    ),
)
