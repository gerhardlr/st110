import pytest
from st110 import st110_LWS
from .conftest import FakeGetCommand, while_doing


@pytest.mark.usefixtures("faketime")
@pytest.mark.usefixtures("disable_rts_cts")
@pytest.mark.usefixtures("mock_serial_echo")
def test_lws_main(fake_command_input: FakeGetCommand):
    serial = st110_LWS.serial_manager.setup_serial()
    step = st110_LWS.execute_next_step(serial)
    stepper = st110_LWS.Stepper(step)
    fake_command_input.set_next_command("1")
    with while_doing(lambda: fake_command_input.set_next_command("")):
        stepper.step()
    fake_command_input.set_next_command("2")
    with while_doing(lambda: fake_command_input.set_next_command("X")):
        stepper.step()
