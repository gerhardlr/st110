from pathlib import Path
import pytest


from st110.spec import load_spec, execute_spec


@pytest.fixture(name="test_file")
def fxt_test_file() -> Path:
    file = Path(__file__).parent.joinpath("data/spec.xlsx")
    return file


def test_load(test_file: Path):
    spec = load_spec(test_file)
    for step in spec.get_next_step():
        assert all(
            [
                step["Detector"] == 6,
                step["HeadNo"] == 0,
                step["PGno"] == 1,
                step["RequiredPRI"] == 100,
                step["RequiredPulsewidth"] == 50,
            ]
        )


@pytest.mark.usefixtures("faketime")
@pytest.mark.usefixtures("disable_rts_cts")
@pytest.mark.usefixtures("set_default_enter")
@pytest.mark.usefixtures("mock_serial_echo")
def test_execute_spec(test_file: Path):
    execute_spec(test_file)
