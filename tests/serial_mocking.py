import os
import pty
from queue import Queue
from threading import Event, Thread
from typing import Literal, Union


def readline(port, active: Event, buffer: "Queue[bytes]"):
    # continuously listen to commands on the master device
    while active.is_set():
        data = b""
        while not data.endswith(b"\r\n"):
            # keep reading one byte at a time until we have a full line
            try:
                data += os.read(port, 1)
            except OSError:
                if not active.is_set():
                    return
        # place the data in a buffer
        buffer.put(data)


class Listener:
    def __init__(self) -> None:
        self._active = Event()
        self._thread = None
        self._data: Queue[Union[bytes, Literal["Stop"]]] = Queue()

    def start(self, port: int):
        self._active.set()
        self._thread = Thread(
            target=readline, args=[port, self._active, self._data], daemon=True
        )
        self._thread.start()

    def get_next_message(self):
        return self._data.get()

    @property
    def listening(self) -> bool:
        if self._thread:
            if self._thread.is_alive():
                return True
        return False

    def signal_self_stop(self):
        self._data.put_nowait("Stop")

    def stop(self):
        if self._thread:
            self._active.clear()


class SerialMock:
    def __init__(self) -> None:
        self._master = None
        self._slave = None
        self._listener = Listener()

    def activate(self):
        master, slave = pty.openpty()
        self._master = master
        self._slave = slave
        self._listener.start(self._master)

    def incoming_messages(self):
        assert self._listener.listening
        while self._listener.listening:
            message = self._listener.get_next_message()
            if message == "Stop":
                return
            yield message

    def signal_self_stop(self):
        self._listener.signal_self_stop()

    def send(self, message: bytes):
        assert self._master
        assert self._listener.listening
        os.write(self._master, message)

    def deactivate(self):
        self._listener.stop()
        if self._slave:
            os.close(self._slave)
        if self._master:
            os.close(self._master)

    @property
    def port(self):
        if self._slave:
            return os.ttyname(self._slave)


class Echo:
    def __init__(self, serial: SerialMock) -> None:
        self._serial = serial
        self._active = Event()
        self._thread = Thread(target=self._echo_thread, daemon=True)

    def _echo_thread(self):
        while self._active.is_set():
            for message in self._serial.incoming_messages():
                if message:
                    self._serial.send(message)

    def start(self):
        self._active.set()
        self._thread.start()

    def stop(self):
        self._active.clear()
        self._serial.signal_self_stop()
        self._thread.join(0.5)
