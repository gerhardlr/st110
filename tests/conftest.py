from contextlib import contextmanager
import datetime
from queue import Queue
from threading import Thread
from typing import Callable, Union, cast
from unittest.mock import patch
import pytest
from serial import Serial
from .serial_mocking import SerialMock, Echo
from st110 import config


@pytest.fixture(name="mock_serial")
def fxt_mock_serial():
    serial = SerialMock()
    serial.activate()
    port = cast(str, serial.port)
    config.set_port_name(port)
    assert port
    yield serial
    serial.deactivate()


@pytest.fixture(name="mock_serial_echo")
def fxt_mock_serial_echo(mock_serial: SerialMock):
    echo = Echo(mock_serial)
    echo.start()
    yield
    echo.stop()


class FakeGetCommand:
    def __init__(self) -> None:
        self._next_command: Queue[str] = Queue()
        self._default_command: Union[None, str] = None

    def set_next_command(self, command: str):
        self._next_command.put(command)

    def get_next_command(self) -> str:
        if not self._next_command.empty():
            command = self._next_command.get()
            return command
        if self._default_command is not None:
            return self._default_command
        raise Exception("No user command entered")

    def set_default_command(self, command: str):
        self._default_command = command


@pytest.fixture(name="fake_command_input")
def fxt_fake_command_input():
    fake_get_command = FakeGetCommand()

    def fake_get(prompt: str):
        print(prompt)
        return fake_get_command.get_next_command()

    with patch("st110.utils.PromptEnter") as fake_command_input:
        fake_command_input.side_effect = fake_get
        yield fake_get_command


@pytest.fixture(name="set_default_enter")
def fxt_set_default_enter(fake_command_input: FakeGetCommand):
    fake_command_input.set_default_command("")


@contextmanager
def while_doing(task: Callable[[], None]):
    thread = Thread(target=task, daemon=True)
    thread.start()
    yield
    thread.join()


@pytest.fixture(name="faketime")
def fxt_faketime():
    with patch("st110.serial_manager.time"):
        with patch("st110.st110_LWS.time"):
            yield


def readline(serial: Serial, timeout: float = 5):
    data = b""
    start_time = datetime.datetime.now()
    end_loop = False
    while not end_loop:
        # read the response
        data += serial.read()
        if data.endswith(b"\r\n"):
            end_loop = True
        else:
            elapsed_time = (datetime.datetime.now() - start_time).total_seconds()
            if elapsed_time > timeout:
                end_loop = True
    return data


@pytest.fixture(name="disable_rts_cts")
def fxt_disable_rts_cts():
    config.disable_rts_cts()
    yield
    config.enable_rts_cts()


@pytest.fixture(name="mocked_serial_port")
def fxt_mocked_serial_port(mock_serial: SerialMock) -> str:
    port = cast(str, mock_serial.port)
    assert port
    return port
